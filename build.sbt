name := "Homework3"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies ++= Seq(
  // Spark core
  "org.apache.spark" % "spark-core_2.12" % "2.4.0",
  // Spark sql
  "org.apache.spark" % "spark-sql_2.12" % "2.4.3",
  // Spark streaming
  "org.apache.spark" % "spark-streaming_2.12" % "2.4.0",
  // Scala library
  "org.scala-lang" % "scala-library" % "2.13.0",
//  // Scala Http
//  "org.scalaj" %% "scalaj-http" % "2.4.2",
  // Logback
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  // slf4j
  "org.slf4j" % "slf4j-log4j12" % "1.7.25",
//  // Typesafe
//  "com.typesafe" % "config" % "1.3.4",
  // Scala test
  "org.scalatest" %% "scalatest" % "3.0.8" % "test"
//  // Scala mock
//  "org.scalamock" %% "scalamock" % "4.4.0" % Test
)
