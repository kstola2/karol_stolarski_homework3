import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.scalatest.FlatSpec

class TestUtils extends FlatSpec {

  val apiKey = "QKLYRF1P9DRDYENS"
  val url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&outputsize=full&apikey=" + apiKey + "&datatype=csv"

  "getCSVData" should "do stuff" in {

    // Arrange

    // Act
    val csvData = MyUtils.getCSVData(url)

    // Assert
    assert(csvData.size > 0)
  }

  "parallelize" should "do stuff" in {

    // Arrange
    val csvData = MyUtils.getCSVData(url)
    val sc = new SparkContext("local", "homework3")
    val spark = SparkSession.builder().getOrCreate()

    // Act
    val ds = MyUtils.parallelize(spark, csvData)

    // Assert
    assert(!ds.isEmpty)

    // Tear Down
    sc.stop()
  }

  "toFrame" should "do stuff" in {
    // Arrange
    val csvData = MyUtils.getCSVData(url)
    val sc = new SparkContext("local", "homework3")
    val spark = SparkSession.builder().getOrCreate()
    val ds = MyUtils.parallelize(spark, csvData)

    // Act
    val df = MyUtils.toFrame(spark, ds)

    // Assert
    assert(!df.isEmpty)

    // Tear Down
    sc.stop()
  }
}
