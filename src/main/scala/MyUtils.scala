import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

import scala.io.Source._

object MyUtils {

  def getCSVData(url: String): Seq[String] = {

    fromURL(url).getLines().toSeq
  }

  def parallelize(spark: SparkSession, data: Seq[String]): Dataset[String] = {
    import spark.implicits._
    spark.sparkContext.parallelize(data).toDS
  }

  def toFrame(spark: SparkSession, ds: Dataset[String]): DataFrame = {
    spark.read.option("header", true).option("inferSchema", true).csv(ds)
  }

  def getSymbolsAndWeights(listOfCompanies: String, funds: Long): Seq[(String, Float)] = {
    val symbols: Array[String] = listOfCompanies.split(",")
    val distro: Float = funds / symbols.size
    symbols.map(sym => sym -> distro).toSeq
  }

  def formatAlphaAdvantageURL(apiKey: String, symbol: String): String = {
    "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&outputsize=full&apikey=" + apiKey + "&datatype=csv"
  }
}
