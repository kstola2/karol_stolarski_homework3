import java.net.UnknownHostException

import org.apache.spark.{SparkContext, rdd}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object MyDriver extends App {
  // API Key
  val apiKey = "QKLYRF1P9DRDYENS"

  try {
    // Funds
    val funds = args(0).toDouble

    // Symbols
    val symbols = args(1).split(",").map(_.trim).toList
    val numSymbols = symbols.size

    //    // Allocate funds
    //    val portfolioMap = Map((symbols, List.fill(numSymbols)(funds / numSymbols)))

    // Spark initialization
    val spark = SparkSession.builder().master("local").appName("MyDriver").getOrCreate()
    import spark.implicits._

    // Dataframes
    val dataframes = symbols.map(stock => {
      val url = MyUtils.formatAlphaAdvantageURL(apiKey, stock)
      val csvData = MyUtils.getCSVData(url)
      val ds = MyUtils.parallelize(spark, csvData)
      MyUtils.toFrame(spark, ds)
    })

    // Perform simulation
    val initialFund: Double = funds / numSymbols
    val initialNumStocks: Double = 0.0D
    val total: Double = dataframes.map(dataframe => {
      val ds = dataframe.select("timestamp", "high", "low").orderBy("timestamp").collect()
      val res: (Double, Double) = ds.aggregate((initialFund, initialNumStocks))((portfolio, row) => {
        val low: Double = row.getAs("low")
        val high: Double = row.getAs("high")
        val numStocksIfBuy = portfolio._1 / low
        val profitIfSell = portfolio._2 * high
        if ((numStocksIfBuy > portfolio._2) && ((numStocksIfBuy * low) <= portfolio._1)) (-(numStocksIfBuy * low), numStocksIfBuy)
        else if ((profitIfSell > portfolio._1) && ((profitIfSell / high) <= portfolio._2)) (profitIfSell, -(profitIfSell / high))
        else (0.0, 0.0)
      }, null)
      res._1
    }).sum

    println(total)

    // Terminate Spark context
    spark.sparkContext.stop()
  } catch {
    case _: ArrayIndexOutOfBoundsException => {
      println(
        """
          |Missing arguments
          |First argument is funds in USD
          |Second argument is a comma-separated list of symbols (companies)
          |""".stripMargin)
    }
    case _: UnknownHostException => {
      println("Failed to retrieve data via API")
    }
    case _: NumberFormatException => {
      println("Funds must be a Long type")
    }
  }
}